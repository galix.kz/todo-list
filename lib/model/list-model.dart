import 'package:cloud_firestore/cloud_firestore.dart';

import 'element.dart';
enum ListStatus{
  active,
  archived
}
class ListModel {
  String color;
  String id;
  int date;
  String name;
  List<String?> users;
  List<String?> owner;
  int? status;
  // List<ElementTask?> tasks;

  ListModel(
      {required this.name,
      required this.date,
      required this.owner,
      required this.users,
        required this.id, this.status,
      required this.color});

  Map<String, dynamic> toMap() {
    return {
      'color': color,
      'date': date,
      'name': name,
      'owner': owner,
      'users': users,
      'status': status
      // 'tasks': tasks,
    };
  }

  ListModel.fromDocumentSnapshot(DocumentSnapshot<Map<String, dynamic>> doc)
      : color = doc.data()!['color'],
        date = doc.data()!['date'],
        name = doc.data()!['name'],
        status = doc.data()!['status'],
        owner = List<String?>.from(doc.data()!['owner']),
        id = doc.data()!['id'],
        users = List<String?>.from(doc.data()!['users']);

  // tasks = doc['tasks'];

  ListModel.fromQueryDocumentSnapshot(QueryDocumentSnapshot<Object?> doc)
      : color = doc['color'],
        date = doc['date'],
        id = doc['id'],
        name = doc['name'],
        status = doc['status'],
        owner = List<String?>.from(doc['owner']),
        users = List<String?>.from(doc["users"]);

// tasks = (doc['tasks'] as List<dynamic>).map((e) => ElementTask.fromMap(e)).toList();

}
