import 'package:cloud_firestore/cloud_firestore.dart';

class ElementTask{
  String id;
  String name;
  bool isDone;

  ElementTask(this.name, this.isDone, this.id);
  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'id': id,
      'isDone': isDone,
    };
  }

  ElementTask.fromMap(Map<String, dynamic> addressMap):
    name = addressMap['name'],
    id = addressMap['id'],
    isDone = addressMap['isDone'];

  ElementTask.fromQueryDocumentSnapshot(QueryDocumentSnapshot<Object?> addressMap):
        name = addressMap['name'],
        id = addressMap['id'],
        isDone = addressMap['isDone'];

}