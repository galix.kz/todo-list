import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel{
  String uid;
  String email;
  String? displayName;

  UserModel({required this.email, required this.uid, required this.displayName});
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'displayName': displayName,
    };
  }

  UserModel.fromMap(Map<String, dynamic> addressMap):
        uid = addressMap['uid'],
        email = addressMap['email'],
        displayName = addressMap['displayName'];

  UserModel.fromQueryDocumentSnapshot(QueryDocumentSnapshot<Object?> addressMap):
        uid = addressMap['uid'],
        email = addressMap['email'],
        displayName = addressMap['displayName'];

}