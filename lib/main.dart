import 'dart:async';


import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todo/gates/auth_gate.dart';
import 'package:todo/ui/screens/page_addlist.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:todo/ui/screens/page_done.dart';
import 'package:todo/ui/screens/page_home.dart';
import 'package:todo/ui/screens/page_settings.dart';
import 'package:todo/ui/screens/page_task.dart';
import 'firebase_options.dart';

Future<Null> main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(TaskistApp());
}


class TaskistApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Taskist",

      home: AuthGate(),
      theme: ThemeData(primarySwatch: Colors.blue),
    );
  }
}
