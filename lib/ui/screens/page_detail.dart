import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todo/model/list-model.dart';
import 'package:todo/ui/widgets/page_detailed/list_header.dart';
import 'package:todo/ui/widgets/list_card.dart';
import 'package:uuid/uuid.dart';

import '../../model/element.dart';
import '../../utils/diamond_fab.dart';
import '../widgets/page_detailed/task_card.dart';

class DetailPage extends StatefulWidget {
  final String id;
  final ListModel card;
  final Map<String, List<ElementTask>> currentList;
  final String color;

  DetailPage(
      {Key? key,
      required this.id,
      required this.card,
      required this.currentList,
      required this.color})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  TextEditingController itemController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          _getToolbar(context),
          Container(
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowGlow();
                return true;
              },
              child: FutureBuilder<DocumentSnapshot>(
                  future: FirebaseFirestore.instance
                      .collection('list')
                      .doc(widget.id)
                      .get(),
                  builder: (BuildContext context,
                      AsyncSnapshot<DocumentSnapshot> snapshot) {
                    if (!snapshot.hasData)
                      return new Center(
                          child: CircularProgressIndicator(
                        backgroundColor: currentColor,
                      ));
                    return new Container(
                      child: getExpenseItems(snapshot),
                    );
                  }),
            ),
          ),
        ],
      ),
      floatingActionButton: DiamondFab(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        autofocus: true,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: currentColor)),
                            labelText: "Item",
                            hintText: "Item",
                            contentPadding: const EdgeInsets.only(
                                left: 16.0,
                                top: 20.0,
                                right: 16.0,
                                bottom: 5.0)),
                        controller: itemController,
                        style: const TextStyle(
                          fontSize: 22.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                        ),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.sentences,
                      ),
                    )
                  ],
                ),
                actions: <Widget>[
                  ButtonTheme(
                    //minWidth: double.infinity,
                    child: RaisedButton(
                      elevation: 3.0,
                      onPressed: () {
                        if (itemController.text.isNotEmpty &&
                            !widget.currentList.values
                                .contains(itemController.text.toString())) {
                          String uid = Uuid().v4();
                          ElementTask task = ElementTask(
                            itemController.text.toString(),
                            false,
                            uid,
                          );
                          FirebaseFirestore.instance
                              .collection('list')
                              .doc(widget.id)
                              .collection('tasks')
                              .doc(uid)
                              .set(task.toMap());

                          itemController.clear();
                          Navigator.of(context).pop();
                        }
                      },
                      child: Text('Add'),
                      color: currentColor,
                      textColor: const Color(0xffffffff),
                    ),
                  )
                ],
              );
            },
          );
        },
        child: Icon(Icons.add),
        backgroundColor: currentColor,
        foregroundColor: currentColor,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  getExpenseItems(AsyncSnapshot<DocumentSnapshot> snapshot) {
    ListModel card = ListModel.fromDocumentSnapshot(
        snapshot.data as DocumentSnapshot<Map<String, dynamic>>);
    List<ElementTask> listElement = [];
    FirebaseFirestore.instance
        .collection('list')
        .doc(widget.id)
        .collection('tasks')
        .get()
        .then((value) => listElement = value.docs
            .map((e) => ElementTask.fromQueryDocumentSnapshot(e))
            .toList());

    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 150.0),
          child: Column(
            children: <Widget>[
              ListHeader(card: card, currentColor: currentColor),
              StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestore.instance
                      .collection('list')
                      .doc(widget.id)
                      .collection('tasks')
                      .snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData) {
                      return const SizedBox();
                    }
                    return Column(
                      children: getTasks(snapshot),
                    );
                  }),
            ],
          ),
        ),
      ],
    );
    // }
  }

  getTasks(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<ElementTask> listElement = snapshot.data!.docs
        .map((docSnap) => ElementTask.fromQueryDocumentSnapshot(docSnap))
        .toList();
    int nbIsDone = 0;
    return [
      Padding(
        padding: EdgeInsets.only(top: 5.0, left: 50.0),
        child: Row(
          children: <Widget>[
            Text(
              nbIsDone.toString() +
                  " of " +
                  listElement.length.toString() +
                  " tasks",
              style: TextStyle(fontSize: 18.0, color: Colors.black54),
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 5.0),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                margin: EdgeInsets.only(left: 50.0),
                color: Colors.grey,
                height: 1.5,
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: <Widget>[
            Container(
              color: Color(0xFFFCFCFC),
              child: SizedBox(
                height: MediaQuery.of(context).size.height - 350,
                child: ListView.builder(
                    physics: const BouncingScrollPhysics(),
                    itemCount: listElement.length,
                    itemBuilder: (BuildContext ctxt, int i) {
                      return TaskCard(
                        card: widget.card,
                        currentColor: currentColor,
                        task: listElement[i],
                      );
                    }),
              ),
            ),
          ],
        ),
      )
    ];
  }

  @override
  void initState() {
    super.initState();
    pickerColor = Color(int.parse(widget.color));
    currentColor = Color(int.parse(widget.color));
  }

  late Color pickerColor;
  late Color currentColor;

  ValueChanged<Color>? onColorChanged;

  changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  Padding _getToolbar(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 50.0, left: 20.0, right: 12.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Image(
            width: 35.0,
            height: 35.0,
            fit: BoxFit.cover,
            image: new AssetImage('assets/list.png')),
        RaisedButton(
          elevation: 3.0,
          onPressed: () {
            pickerColor = currentColor;
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Pick a color!'),
                  content: SingleChildScrollView(
                    child: ColorPicker(
                      pickerColor: pickerColor,
                      onColorChanged: changeColor,
                      colorPickerWidth: 200,
                      pickerAreaHeightPercent: 0.7,
                    ),
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Got it'),
                      onPressed: () {
                        FirebaseFirestore.instance
                            .collection('list')
                            .doc(widget.id)
                            .update({"color": pickerColor.value.toString()});

                        setState(() => currentColor = pickerColor);
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          },
          child: Text('Color'),
          color: currentColor,
          textColor: const Color(0xffffffff),
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: new Icon(
            Icons.close,
            size: 40.0,
            color: currentColor,
          ),
        ),
      ]),
    );
  }
}
