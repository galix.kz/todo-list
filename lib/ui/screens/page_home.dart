import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todo/ui/screens/page_done.dart';
import 'package:todo/ui/screens/page_settings.dart';
import 'package:todo/ui/screens/page_task.dart';

late User _currentUser;

class HomePage extends StatefulWidget {
  final User user;


  HomePage({Key? key, required this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 1;


  @override
  void initState() {
    super.initState();
    _currentUser = widget.user;
    _children = [DonePage(
      user: _currentUser,
    ),
      TaskPage(
        user: _currentUser,
      ),
      SettingsPage(
        user: _currentUser,
      )];
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  List<Widget> _children = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        fixedColor: Colors.deepPurple,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon:  Icon(FontAwesomeIcons.calendarCheck), label: ''),
          BottomNavigationBarItem(
              icon:  Icon(FontAwesomeIcons.calendar),  label: ''),
          BottomNavigationBarItem(
              icon:  Icon(FontAwesomeIcons.slidersH),  label: '')

        ],
      ),
      body: _children[_currentIndex],
    );
  }

  @override
  void dispose() {
    super.dispose();
  }


  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}