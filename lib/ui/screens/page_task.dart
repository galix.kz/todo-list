import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todo/model/list-model.dart';
import 'package:todo/ui/screens/page_detail.dart';
import 'package:todo/ui/widgets/list_card.dart';


import '../../model/element.dart';
import 'page_addlist.dart';

class TaskPage extends StatefulWidget {
  final User user;

  TaskPage({Key? key, required this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TaskPageState();
}

class _TaskPageState extends State<TaskPage>
    with SingleTickerProviderStateMixin {
  int index = 1;

  @override
  Widget build(BuildContext context) {

    print(ListStatus.active.index);
    return Scaffold(
      body: ListView(
        children: <Widget>[
          _getToolbar(context),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        color: Colors.grey,
                        height: 1.5,
                      ),
                    ),
                    Expanded(
                        flex: 2,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const <Widget>[
                            Text(
                              'Task',
                              style: TextStyle(
                                  fontSize: 30.0, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Lists',
                              style: TextStyle(
                                  fontSize: 28.0, color: Colors.grey),
                            )
                          ],
                        )),
                    Expanded(
                      flex: 1,
                      child: Container(
                        color: Colors.grey,
                        height: 1.5,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: 50.0,
                      height: 50.0,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black38),
                          borderRadius: const BorderRadius.all(Radius.circular(7.0))),
                      child: IconButton(
                        icon: const Icon(Icons.add),
                        onPressed: _addTaskPressed,
                        iconSize: 30.0,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Text('Add List',
                          style: TextStyle(color: Colors.black45)),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 50.0),
            child: Container(
              height: 360.0,
              padding: const EdgeInsets.only(bottom: 25.0),
              child: NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  overscroll.disallowIndicator();
                  return true;
                },


                child: StreamBuilder<QuerySnapshot>(

                    stream: FirebaseFirestore.instance
                        .collection('list').where('status', isEqualTo: 0).where('users', arrayContains: widget.user.uid)
                        .orderBy("date", descending: true).snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (!snapshot.hasData) {
                        return const Center(
                            child: CircularProgressIndicator(
                          backgroundColor: Colors.blue,
                        ));
                      }

                      return  ListView(
                        physics: const BouncingScrollPhysics(),
                        padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                        scrollDirection: Axis.horizontal,
                        children: getExpenseItems(snapshot),
                      );
                    }),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<ElementTask> listElement = [], listElement2;
    List<ListModel> cardsList;
    Map<String, List<ElementTask>> userMap = {};

    List<String> cardColor = [];

    if (widget.user.uid.isNotEmpty) {
      cardColor.clear();

      cardsList = snapshot.data!.docs.map((docSnap)=> ListModel.fromQueryDocumentSnapshot(docSnap)).toList();
      print(cardsList);
      return List.generate(cardsList.length, (int index) {
        ListModel card = cardsList.elementAt(index);
        var hsl = HSLColor.fromColor(Color(int.parse(card.color)));
        var middleColor = hsl.withLightness((hsl.lightness - 0.01).clamp(0.0, 1.0));

        return
        Container(
          padding: EdgeInsets.all(5),
          child: OpenContainer(closedBuilder: (context, closedContainer){
            return
              ListCard(list: card);
          },
              openColor: middleColor.toColor(),
              middleColor: middleColor.toColor(),
              onClosed: (data){
                middleColor = hsl.withLightness((hsl.lightness + 0.5).clamp(0.0, 1.0));
              },
              closedColor: hsl.toColor(),
              openElevation: 0.0,
              closedElevation: 10.0,
              transitionType: ContainerTransitionType.fadeThrough,
              transitionDuration: Duration(milliseconds: 300),
              closedShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
              openBuilder: (context, openContainer){
                return DetailPage(
                  id: card.id,
                  card: card,
                  currentList: userMap,
                  color: card.color,
                );
              }),
        )
        
          ;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  void _addTaskPressed() async {
    Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (_, __, ___) => NewTaskPage(
              user: widget.user,
            ),
        transitionsBuilder: (context, animation, secondaryAnimation, child) =>
            ScaleTransition(
              scale: Tween<double>(
                begin: 1.5,
                end: 1.0,
              ).animate(
                CurvedAnimation(
                  parent: animation,
                  curve: const Interval(
                    0.50,
                    1.00,
                    curve: Curves.linear,
                  ),
                ),
              ),
              child: ScaleTransition(
                scale: Tween<double>(
                  begin: 0.0,
                  end: 1.0,
                ).animate(
                  CurvedAnimation(
                    parent: animation,
                    curve: const Interval(
                      0.00,
                      0.50,
                      curve: Curves.linear,
                    ),
                  ),
                ),
                child: child,
              ),
            ),
      ),
    );
    //Navigator.of(context).pushNamed('/new');
  }

  Padding _getToolbar(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
      child:
      Row(mainAxisAlignment: MainAxisAlignment.center, children: const [
        Image(
            width: 40.0,
            height: 40.0,
            fit: BoxFit.cover,
            image: AssetImage('assets/list.png')
        ),
      ]),
    );
  }
}
