import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todo/model/list-model.dart';

import '../../../model/user_model.dart';

class AddUser extends StatefulWidget {
  final ListModel card;
  final Color currentColor;

  const AddUser({Key? key, required this.card, required this.currentColor})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => AddUserState();
}

class AddUserState extends State<AddUser> {
  static String _displayStringForOption(UserModel option) =>
      option.email;
  UserModel? selectedUser;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Add user to: " + widget.card.name),
              content: SizedBox(
                height: 200,
                child: Column(
                  children: [
                    Autocomplete<UserModel>(
                      displayStringForOption: _displayStringForOption,
                      optionsBuilder:
                          (TextEditingValue textEditingValue) async {
                        if (textEditingValue.text == '') {
                          return const [];
                        }
                        List<UserModel> users = [];
                        QuerySnapshot<Map<String, dynamic>> userSnapshot =
                            await FirebaseFirestore.instance
                                .collection('users')
                                .where('email',
                                    isGreaterThanOrEqualTo:
                                        textEditingValue.text.toLowerCase(),
                                    isLessThan:
                                        textEditingValue.text.toLowerCase() +
                                            'z')
                                .get();
                        userSnapshot.docs.forEach((e) {
                          users.add(UserModel.fromQueryDocumentSnapshot(e));
                        });

                        return users;
                      },
                      onSelected: (UserModel selection) {
                        selectedUser = selection;
                      },
                    ),
                    SizedBox(height: 10,),
                    StreamBuilder<QuerySnapshot>(
                        stream: FirebaseFirestore.instance
                            .collection('users')
                            .where('uid', whereIn: widget.card.users)
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (!snapshot.hasData) {
                            return const SizedBox();
                          }
                          return
                            SizedBox(
                              height: 100,
                              child: Column(
                                children: getUsers(snapshot),
                              ),
                            );
                        }),
                  ],
                ),
              ),
              actions: <Widget>[
                ButtonTheme(
                  //minWidth: double.infinity,
                  child: RaisedButton(
                    elevation: 3.0,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Cancel'),
                    color: widget.currentColor,
                    textColor: const Color(0xffffffff),
                  ),
                ),
                ButtonTheme(
                  //minWidth: double.infinity,
                  child: RaisedButton(
                    elevation: 3.0,
                    onPressed: () {
                      if(selectedUser!=null) {
                        FirebaseFirestore.instance
                          .collection('list')
                          .doc(widget.card.id).update({
                        'users': FieldValue.arrayUnion([selectedUser!.uid])
                      });
                      }
                      Navigator.pop(context);

                    },
                    child: Text('Add'),
                    color: widget.currentColor,
                    textColor: const Color(0xffffffff),
                  ),
                ),
              ],
            );
          },
        );
      },
      child: Icon(
        FontAwesomeIcons.person,
        size: 25.0,
        color: widget.currentColor,
      ),
    );
  }

  getUsers(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<Map<String, dynamic>> listElement = [];
    List<UserModel> usersList = [];

    usersList = snapshot.data!.docs
        .map((docSnap) => UserModel.fromQueryDocumentSnapshot(docSnap))
        .toList();
    return List.generate(usersList.length, (int index) {
      UserModel user = usersList.elementAt(index);

      return Text(user.email);
    });
  }
}
