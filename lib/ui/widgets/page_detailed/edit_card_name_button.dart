import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../model/element.dart';
import '../../../model/list-model.dart';

class EditCardNameButton extends StatefulWidget {
  final ListModel card;
  final Color currentColor;

  const EditCardNameButton(
      {super.key, required this.card, required this.currentColor});

  @override
  State<StatefulWidget> createState() => _EditCardNameButton();
}

class _EditCardNameButton extends State<EditCardNameButton> {


  @override
  Widget build(BuildContext context) {
    TextEditingController itemController = TextEditingController(text: widget.card.name);
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: widget.currentColor)),
                          labelText: "Name",
                          hintText: "Name",
                          contentPadding: const EdgeInsets.only(
                              left: 16.0, top: 20.0, right: 16.0, bottom: 5.0)),
                      controller: itemController,
                      style: const TextStyle(
                        fontSize: 22.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.sentences,
                    ),
                  )
                ],
              ),
              actions: <Widget>[
                ButtonTheme(
                  //minWidth: double.infinity,
                  child: RaisedButton(
                    elevation: 3.0,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('No'),
                    color: widget.currentColor,
                    textColor: const Color(0xffffffff),
                  ),
                ),
                ButtonTheme(
                  //minWidth: double.infinity,
                  child: RaisedButton(
                    elevation: 3.0,
                    onPressed: () {
                      if (itemController.text.isNotEmpty) {
                        FirebaseFirestore.instance.collection('list').doc(widget.card.id).update({
                          "name": itemController.text.toString()
                        });
                        itemController.clear();
                        Navigator.of(context).pop();
                      }
                    },
                    color: widget.currentColor,
                    textColor: const Color(0xffffffff),
                    child: const Text('update'),
                  ),
                ),

              ],
            );
          },
        );
      },
      child: Icon(
        FontAwesomeIcons.pencil,
        size: 25.0,
        color: widget.currentColor,
      ),
    );
  }
}
