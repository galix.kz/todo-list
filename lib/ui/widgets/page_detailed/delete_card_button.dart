import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../model/element.dart';
import '../../../model/list-model.dart';

class DeleteCardButton extends StatelessWidget{
  final ListModel card;
  final Color currentColor;

  const DeleteCardButton({super.key, required this.card, required this.currentColor});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return new AlertDialog(
              title: Text("Delete: " + card.name),
              content: Text(
                "Are you sure you want to delete this list?", style: TextStyle(fontWeight: FontWeight.w400),),
              actions: <Widget>[
                ButtonTheme(
                  //minWidth: double.infinity,
                  child: RaisedButton(
                    elevation: 3.0,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('No'),
                    color: currentColor,
                    textColor: const Color(0xffffffff),
                  ),
                ),
                ButtonTheme(
                  //minWidth: double.infinity,
                  child: RaisedButton(
                    elevation: 3.0,
                    onPressed: () {
                      FirebaseFirestore.instance
                          .collection('list')
                          .doc(card.id)
                          .update({'status': ListStatus.archived.index});
                      Navigator.pop(context);
                      Navigator.of(context).pop();
                    },
                    child: Text('YES'),
                    color: currentColor,
                    textColor: const Color(0xffffffff),
                  ),
                ),
              ],
            );
          },
        );
      },
      child: Icon(
        FontAwesomeIcons.trash,
        size: 25.0,
        color: currentColor,
      ),
    );
  }

}