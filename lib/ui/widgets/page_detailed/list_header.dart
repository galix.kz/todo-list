import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todo/model/list-model.dart';
import 'package:todo/ui/widgets/page_detailed/add_user.dart';
import 'package:todo/ui/widgets/page_detailed/delete_card_button.dart';
import 'package:todo/ui/widgets/page_detailed/edit_card_name_button.dart';

class ListHeader extends StatelessWidget{
  final ListModel card;
  final Color currentColor;
  const ListHeader({Key? key, required this.card, required this.currentColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.only(top: 5.0, left: 50.0, right: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            fit: FlexFit.loose,
            child: Text(
              card.name,
              softWrap: true,
              overflow: TextOverflow.fade,
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 35.0),
            ),
          ),
          EditCardNameButton(card: card, currentColor: currentColor),
          AddUser(card: card, currentColor: currentColor),
          DeleteCardButton(card: card, currentColor: currentColor)
        ],
      ),
    );
  }

}