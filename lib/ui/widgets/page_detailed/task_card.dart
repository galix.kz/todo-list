import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todo/model/element.dart';
import 'package:todo/model/list-model.dart';

class TaskCard extends StatelessWidget {
  final ElementTask task;
  final ListModel card;
  final Color currentColor;

  const TaskCard(
      {Key? key, required this.task, required this.currentColor, required this.card})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController itemController = TextEditingController(
        text: task.name);
    return Slidable(
      // delegate: new SlidableBehindDelegate(),
      // actionExtentRatio: 0.25,
      endActionPane: ActionPane(
        motion: ScrollMotion(),
        children: [
          SlidableAction(
            // An action can be bigger than the others.
            label: 'Edit',
            backgroundColor: Colors.blue,
            foregroundColor: Colors.white,
            icon: Icons.edit,
            onPressed: (BuildContext context) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                        content: Row(
                          children: <Widget>[
                            Expanded(
                              child: TextField(
                                autofocus: true,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: currentColor)),
                                    labelText: "Name",
                                    hintText: "Name",
                                    contentPadding: const EdgeInsets.only(
                                        left: 16.0,
                                        top: 20.0,
                                        right: 16.0,
                                        bottom: 5.0)),
                                controller: itemController,
                                style: const TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization
                                    .sentences,
                              ),
                            )
                          ]
                          ,
                        ),
                      actions: <Widget>[
                        ButtonTheme(
                          //minWidth: double.infinity,
                          child: RaisedButton(
                            elevation: 3.0,
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text('No'),
                            color: currentColor,
                            textColor: const Color(0xffffffff),
                          ),
                        ),
                        ButtonTheme(
                          //minWidth: double.infinity,
                          child: RaisedButton(
                            elevation: 3.0,
                            onPressed: () {
                              FirebaseFirestore.instance
                                  .collection('list')
                                  .doc(card.id)
                                  .collection('tasks').doc(task.id).update({
                                'name': itemController.text.toString()
                              });
                              Navigator.pop(context);
                            },
                            child: Text('Save'),
                            color: currentColor,
                            textColor: const Color(0xffffffff),
                          ),
                        ),
                      ],
                    );
                  });
            },
          ),
          SlidableAction(
            // An action can be bigger than the others.
            label: 'Delete',
            backgroundColor: Colors.red,
            foregroundColor: Colors.white,
            icon: Icons.delete,
            onPressed: (BuildContext context) {
              FirebaseFirestore.instance.collection('list').doc(card.id)
                  .collection('tasks').doc(task.id)
                  .delete();
            },
          ),

        ],
      ),
      // delegate: new SlidableBehindDelegate(),
      // actionExtentRatio: 0.25,
      child: GestureDetector(
        onTap: () {
          FirebaseFirestore.instance
              .collection('list')
              .doc(card.id)
              .collection('tasks').doc(task.id)
              .update({
            "isDone": !task.isDone
          });
        },
        child: Container(
          height: 50.0,
          color: task.isDone
              ? Color(0xFFF0F0F0)
              : Color(0xFFFCFCFC),
          child: Padding(
            padding: EdgeInsets.only(left: 50.0),
            child: Row(
              mainAxisAlignment:
              MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  task.isDone
                      ? FontAwesomeIcons.checkSquare
                      : FontAwesomeIcons.square,
                  color: task.isDone
                      ? currentColor
                      : Colors.black,
                  size: 20.0,
                ),
                const Padding(
                  padding:
                  EdgeInsets.only(left: 30.0),
                ),
                Flexible(
                  child: Text(
                    task.name,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: task
                        .isDone
                        ? TextStyle(
                      decoration: TextDecoration
                          .lineThrough,
                      color: currentColor,
                      fontSize: 27.0,
                    )
                        : const TextStyle(
                      color: Colors.black,
                      fontSize: 27.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}