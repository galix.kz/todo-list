
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../model/element.dart';
import '../../model/list-model.dart';

class ListCard extends StatelessWidget {
  ListModel list;

  ListCard({Key? key, required this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
      ),
      color: Color(int.parse(list.color)),
      child: Container(
        width: 220.0,
        //height: 100.0,
        child: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 20.0, bottom: 15.0),
                child: Container(
                  child: Text(
                    list.name,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 19.0,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Container(
                        margin: const EdgeInsets.only(left: 50.0),
                        color: Colors.white,
                        height: 1.5,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 30.0, left: 15.0, right: 5.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 200.0,
                      child: StreamBuilder<QuerySnapshot>(
                          stream: FirebaseFirestore.instance
                              .collection('list')
                              .doc(list.id)
                              .collection('tasks')
                              .snapshots(),
                          builder: (BuildContext context,
                              AsyncSnapshot<QuerySnapshot> snapshot) {
                            if (!snapshot.hasData) {
                              return const SizedBox();
                            }
                            return Column(
                              children: getTasks(snapshot),
                            );
                          }),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  getTasks(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<ElementTask> listElement = snapshot.data!.docs
        .map((docSnap) => ElementTask.fromQueryDocumentSnapshot(docSnap))
        .toList();
    return [
      SizedBox(
        height: 200,
        child: ListView.builder(
          //physics: const NeverScrollableScrollPhysics(),
            itemCount: listElement.length,
            itemBuilder: (BuildContext ctxt, int i) {
              ElementTask task = listElement.elementAt(i);
              return Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    task.isDone
                        ? FontAwesomeIcons.checkCircle
                        : FontAwesomeIcons.circle,
                    color: task.isDone ? Colors.white70 : Colors.white,
                    size: 14.0,
                  ),
                  const Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                  ),
                  Flexible(
                    child: Text(
                      task.name,
                      style: task.isDone
                          ? const TextStyle(
                        decoration: TextDecoration.lineThrough,
                        color: Colors.white70,
                        fontSize: 17.0,
                      )
                          : const TextStyle(
                        color: Colors.white,
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                ],
              );
            }),
      )
      ];
  }
}
