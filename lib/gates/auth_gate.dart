import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:todo/ui/screens/page_home.dart';
import 'package:flutterfire_ui/auth.dart';

class AuthGate extends StatelessWidget {
  const AuthGate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        return StreamBuilder<User?>(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            // User is not signed in
            if (!snapshot.hasData) {
              return SignInScreen(
                  providerConfigs: [
                    EmailProviderConfiguration(),
                    GoogleProviderConfiguration(
                      clientId: '279589693033-b0ugifhk6ahm4e4vg1vtn7k9bhm03t2n.apps.googleusercontent.com',
                    ),
                  ]
              );
            }
            User user = snapshot.data!;
            FirebaseFirestore.instance.collection('users').doc(snapshot.data!.uid).set({
              'uid': user.uid,
              'email': user.email,
              'displayName': user.displayName,
            });
            // Render your application if authenticated
            return HomePage(user: snapshot.data!);
          },
        );
      },
    );
  }
}